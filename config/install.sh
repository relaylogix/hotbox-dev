#!/bin/bash
# see if smoked exists
if [ ! -f ~/.smoke/smoked ]; then 
	cd ~/.smoke

	# download smoked and wallet
	wget https://github.com/smokenetwork/smoked/releases/download/v0.1.0/smoked-0.1.0-x86_64-linux.tar.gz
	wget https://github.com/smokenetwork/smoked/releases/download/v0.0.6/cli_wallet-0.0.6-x86_64-linux.tar.gz
	
	# extract smoked and wallet
	tar -xzf smoked-0.1.0-x86_64-linux.tar.gz
	tar -xzf cli_wallet-0.0.6-x86_64-linux.tar.gz
	
	# remove tar files
	rm *.gz
	
	# wait two seconds
	sleep 2

	# Launch script in background
	~/.smoke/smoked &
	# Get its PID
	smokePID=$!
	# Wait for 2 seconds
	sleep 2
	# Kill it
	kill -INT $smokePID
	sleep 4
	# move preset config
	cp ~/.config/config.ini.example witness_node_data_dir/config.ini
	# move to home dir
	cd
fi

# run tmux
# the next line isolated for dev purposes (Testing Multiple Tmux sessions at startup) $7-20-20
#tmux new -s hotbox (UNCOMMENT THIS LINE FOR ORIGINAL SCRIPT)

# this code added to attempt multi session Tmux at startup. ------------- $7-20-20
# code source github hotbox install.sh by @jrswab

# run tmux with multiple window created.
session="Hotbox | ctrl+h to leave | ctrl+b then a number to swich windows"

# set up tmux
tmux start-server

# create a new tmux session, starting vim from a saved session in the new window
tmux new-session -d -s "$session" -n smoke

# create a new window called scratch
tmux new-window -t "$session":1 -n wallet

# create a new window called scratch
tmux new-window -t "$session":2 -n kief

# return to main vim window
tmux select-window -t "$session":0

# Finished setup, attach to the tmux session!
tmux attach-session -t "$session"

# this ends the code added for multi session Tmux at startup. -----------$7-20-20


exit
